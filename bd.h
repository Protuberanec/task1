#ifndef BD_H
#define BD_H

#include <QObject>
#include    <QString>
#include    <QDebug>
#include    <QtSql>
#include    <QSqlQuery>
#include    <QSqlTableModel>

class BD : public QObject
{
    Q_OBJECT
private :
    QSqlDatabase my_bd;
    QSqlQuery *query;
    QStringList table_name;

    void open_bd(QString file_name_bd);

    quint32 count_graph;
    QMap<QString, double> *graph;
    QMap<QString, double> all_graph;

    quint16 GetCountGraph(QString table_name);

    quint32 GetCountTable();
public:
    explicit BD(QObject *parent = nullptr);
    BD(QString file_name_bd);
    ~BD();

    quint32 ReadAllData(QString table_name);

    QMap<QString, double> GetData(quint32 num_graph);
    QMap<QString, double> GetAllData();

    QStringList ReturnTableName() {
        return table_name;
    }

signals:

};

#endif // BD_H
