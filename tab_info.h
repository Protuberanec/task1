#ifndef TAB_INFO_H
#define TAB_INFO_H

#include <QObject>
#include    <QWidget>
#include    <QTableWidget>
#include    <QHBoxLayout>
#include    <QPushButton>

#include    "qcustomplot.h"

class TAB_INFO : public QObject
{
    Q_OBJECT
private :

    QHBoxLayout *Hbox;
    QTableWidget *bd_table;
    QCustomPlot *plot;

    void CreateComp();
    void PlaceComp();
    void CreateTable(QStringList header_name);

public:
    explicit TAB_INFO(QWidget *wdgt = nullptr, QObject *parent = nullptr);
    ~TAB_INFO();

    void AddRows(QMap<QString, double> data);
    void AddGraph(QMap<QString, double> data);

signals:

};

#endif // TAB_INFO_H
