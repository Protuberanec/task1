#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    open_dialog = new QFileDialog();
    curr_bd = nullptr;

    connect(ui->actionopen_file_bd, &QAction::triggered, this, &MainWindow::slot_OpenBd);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ReadData(QString name_tab)
{
    quint32 count_graph = curr_bd->ReadAllData(name_tab);
    AddTab(name_tab);

    QMap<QString, double> graph_data = curr_bd->GetAllData();
    tabs.last()->AddGraph(graph_data);
    tabs.last()->AddRows(graph_data);
}

void MainWindow::slot_OpenBd()
{
    QString file_name = open_dialog->getOpenFileName();
    if(file_name.size() == 0)
        return;

    CloseTabs();
    open_read_bd(file_name);
}

void MainWindow::open_read_bd(QString name_bd)
{
    if (curr_bd)
        delete curr_bd;

    curr_bd = new class BD(name_bd);

    QStringList table_name = curr_bd->ReturnTableName();
    for (int i = 0; i < table_name.size(); i++) {
        ReadData(table_name.at(i)); //assume all table 一样
    }
}

void MainWindow::close_bd()
{
    delete curr_bd;
}

void MainWindow::AddTab(QString tab_name)
{
    QWidget *new_tab = new QWidget();
    TAB_INFO *tab_bd_table = new TAB_INFO(new_tab);

    tabs.append(tab_bd_table);

    ui->TW_Info->addTab(new_tab, tab_name);
}

void MainWindow::CloseTabs()
{
    quint32 i = 0;
    while( tabs.size() )
    {
        ui->TW_Info->removeTab(0);
        delete tabs.at(0);
        tabs.removeFirst();
        i++;
    }
}

