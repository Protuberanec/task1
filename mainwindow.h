#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include    <QFileDialog>
#include    "bd.h"
#include "tab_info.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void open_read_bd(QString name_bd);

private:
    Ui::MainWindow *ui;
    BD *curr_bd;

    QList<TAB_INFO*> tabs;

    QFileDialog *open_dialog;


    void close_bd();
    void AddTab(QString tab_name);
    void CloseTabs();
    void ReadData(QString name_tab);

private slots:
    void slot_OpenBd();

};
#endif // MAINWINDOW_H
