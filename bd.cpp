#include "bd.h"

quint16 BD::GetCountGraph(QString table_name)
{
    QString sql_request = QString("select COUNT(DISTINCT %1.Number) from %1;").arg(table_name);
    bool result_sql_req = query->exec(sql_request);
    if (result_sql_req == false) {
        return 0;
    }

    QSqlRecord records = query->record();
    query->next();
    return query->value(0).toInt();
}

quint32 BD::GetCountTable()
{
    if (my_bd.isOpen() == false)
        return 0;

    table_name = my_bd.tables();
    return table_name.size();
}

BD::BD(QObject *parent) : QObject(parent)
{
    graph = nullptr;
}

BD::BD(QString file_name_bd)
{
    graph = nullptr;
    qDebug() << tr("bd %1").arg(file_name_bd);
    my_bd = QSqlDatabase::addDatabase("QSQLITE");
    my_bd.setDatabaseName(file_name_bd);
    bool is_opened = my_bd.open();
    qDebug() << tr("bd was opened = %1").arg(is_opened);
    query = new QSqlQuery(my_bd);

    GetCountTable();
}

BD::~BD()
{
    my_bd.close();
    delete query;
    delete [] graph;
}

/*
    description :   this function will return amount of graphs
*/
quint32 BD::ReadAllData(QString table_name)
{
    if (my_bd.isOpen() == false)
        return 0;
    count_graph = GetCountGraph(table_name);
    if (count_graph == 0) {
        return 0;
    }
    if (graph != nullptr)
        delete [] graph;
    graph = new QMap<QString, double> [count_graph];

//    QString sql_request = QString("SELECT datetime(%1.Time, 'unixepoch', 'localtime'), %1.Value, %1.Number from %1 order by %1.Number;").arg(table_name);
    QString sql_request = QString("SELECT datetime(%1.Time, 'unixepoch', 'localtime'), %1.Value, %1.Number from %1;").arg(table_name);
    bool result_sql_req = query->exec(sql_request);
    if (result_sql_req == false) {
        return 0;
    }

    QSqlRecord records = query->record();

    while(query->next()) {
//        graph[query->value("Number").toUInt()].insert(query->value(0).toString(), query->value("Value").toDouble());  //field by group
        all_graph.insert(query->value(0).toString() + tr("_%1").arg(query->value("Number").toUInt()), query->value("Value").toDouble());
    }

    return count_graph;
}

QMap<QString, double> BD::GetData(quint32 num_graph)
{
    if (num_graph > count_graph)
        return QMap<QString, double>();

    return graph[num_graph];
}

QMap<QString, double> BD::GetAllData()
{
    return all_graph;
}









