#include "tab_info.h"

void TAB_INFO::CreateComp()
{
    bd_table = new QTableWidget();
    plot = new QCustomPlot();
    plot->xAxis->setLabel("Время");
    plot->yAxis->setLabel("Значения");
    Hbox = new QHBoxLayout();
}

void TAB_INFO::PlaceComp()
{
    Hbox->addWidget(bd_table);
    Hbox->addWidget(plot);
    plot->resize(1,1);

    Hbox->setStretch(0,40);
    Hbox->setStretch(1,60);
}

void TAB_INFO::CreateTable(QStringList header_name)
{
    bd_table->setColumnCount(header_name.size());
    bd_table->setHorizontalHeaderLabels(header_name);
}

TAB_INFO::TAB_INFO(QWidget *wdgt, QObject *parent) : QObject(parent)
{
    CreateComp();
    PlaceComp();

    QStringList temp_hor_header;
    temp_hor_header << "date time" << "value" << "group";
    CreateTable(temp_hor_header);   //possible to create header outside the class... need to rewrite 一点儿。。。

    wdgt->setLayout(Hbox);
    wdgt->show();
}

TAB_INFO::~TAB_INFO()
{
    delete bd_table;
    delete plot;
    delete Hbox;
}

void TAB_INFO::AddRows(QMap<QString, double> data)
{
    QTableWidgetItem *item;
    QMap<QString, double>::iterator current_row = data.begin();

    QStringList time_sep_group;

    while(current_row != data.end()) {
        time_sep_group = current_row.key().split('_');

        bd_table->setRowCount(bd_table->rowCount() + 1);

        item = new QTableWidgetItem(time_sep_group.at(0));
        bd_table->setItem(bd_table->rowCount() -1 , 0, item);

        item = new QTableWidgetItem(QString::number(current_row.value(), 'f', 6));
        bd_table->setItem(bd_table->rowCount() -1 , 1, item);

        item = new QTableWidgetItem(time_sep_group.at(1));
        bd_table->setItem(bd_table->rowCount() -1 , 2, item);

        current_row++;
    }
}

void TAB_INFO::AddGraph(QMap<QString, double> data)
{
    plot->addGraph();

    QVector<double> coor_x;
//    static quint32 max_x = 0;
    double max_y = 0;
    double min_y = 0;

    QList<double> coor_y = data.values();

//get max and min of the graph...
//to put thread... after end of thread rescale graph...
    for (int i = 0; i < coor_y.size(); i++) {
        coor_x.push_back(i);
        if (max_y < coor_y.at(i))        {
            max_y = coor_y.at(i);
        }
        if (min_y > coor_y.at(i))        {
            min_y = coor_y.at(i);
        }
    }
//------------------------------
    plot->xAxis->setRange(0, coor_x.size());
    plot->yAxis->setRange(min_y - 10, max_y + 10);

    plot->graph(plot->graphCount() - 1)->setData(coor_x, coor_y.toVector());    //if on the form will added additional graph

    plot->xAxis->setTickLabelRotation(90);
//create ticks...
    QSharedPointer<QCPAxisTickerText> my_ticks(new QCPAxisTickerText);
    QMap<QString, double>::iterator iter = data.begin();
    const quint32 step_ticks = 2000;
    for (int i = 0; i < coor_x.size()/step_ticks; i++) {
        my_ticks->addTick(i * step_ticks, iter.key().split('_').at(0)); //.at(1) - placed group of the graph
        iter += step_ticks;
    }
    plot->xAxis->setTicker(my_ticks);
//------------------------------------
    plot->replot();
}
